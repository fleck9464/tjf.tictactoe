#pragma once


#include <iostream>
#include <string>

class TicTacToe {

private:
	//fields
	char m_board[9];
	int m_numTurns;
	char m_playerTurn;
	char m_winner;

public:
	//constructor
	TicTacToe() {

		for (int i = 0; i < 9; i++) { m_board[i] = ' '; }
		m_numTurns = 0;//turn # starts at 0 since incremented before 1st turn
		m_playerTurn = 'X';
		m_winner = ' ';
	}
	//methods
	void DisplayBoard()
	{
		//display board using array
		std::cout << "1    |2    |3    \n";
		std::cout << "  " << m_board[0] << "  |  " << m_board[1] << "  |  " << m_board[2] << '\n';
		std::cout << "_____|_____|_____\n";
		std::cout << "4    |5    |6    \n";
		std::cout << "  " << m_board[3] << "  |  "<< m_board[4] << "  |  " << m_board[5] << '\n';
		std::cout << "_____|_____|_____\n";
		std::cout << "7    |8    |9    \n";
		std::cout << "  " << m_board[6] << "  |  " << m_board[7] << "  |  " << m_board[8] << '\n';
		std::cout << "     |     |     \n";
	}

	bool IsOver()
	{
		
		if (m_numTurns == 9) { return true; }
		//check for win states
		else if (m_board[0] != ' ' && m_board[0] == m_board[1] && m_board[1] == m_board[2]) { return true; }
		else if (m_board[3] != ' ' && m_board[3] == m_board[4] && m_board[4] == m_board[5]) { return true; }
		else if (m_board[6] != ' ' && m_board[6] == m_board[7] && m_board[7] == m_board[8]) { return true; }
		else if (m_board[0] != ' ' && m_board[0] == m_board[3] && m_board[3] == m_board[6]) { return true; }
		else if (m_board[1] != ' ' && m_board[1] == m_board[4] && m_board[4] == m_board[7]) { return true; }
		else if (m_board[2] != ' ' && m_board[2] == m_board[5] && m_board[5] == m_board[8]) { return true; }
		else if (m_board[0] != ' ' && m_board[0] == m_board[4] && m_board[4] == m_board[8]) { return true; }
		else if (m_board[2] != ' ' && m_board[2] == m_board[4] && m_board[4] == m_board[6]) { return true; }
		//use boolean for empty space
		else { 
			// if game not over advance turn count, get player
			m_numTurns++;
			m_playerTurn = GetPlayerTurn();
			return false; 
		}

	}

	char GetPlayerTurn()
	{
		//odd turn # = player 1, even turn # = player 2
		int turn = m_numTurns % 2;
		if (turn == 1) return 'X';
		else return 'O';
	}

	bool IsValidMove(int position)
	{
		if (position < 10 && position > 0)
		{
			//check for space in board array
			if (m_board[position - 1] == ' ') return true;
			else return false;
		}
		else return false;
	}

	void Move(int position)
	{
		m_board[position - 1] = m_playerTurn;
	}

	void DisplayResult()
	{
		if (m_numTurns < 9)
		{
			//if game ends before ninth turn, current player must be winner
			std::cout << m_playerTurn << " is the winner!\n";
		}
		else
		{	//check for win state, else tie game
			if (m_board[0] != ' ' && m_board[0] == m_board[1] && m_board[1] == m_board[2]) { std::cout << m_board[0] << " is the winner!\n"; }
			else if (m_board[3] != ' ' && m_board[3] == m_board[4] && m_board[4] == m_board[5]) { std::cout << m_board[3] << " is the winner!\n"; }
			else if (m_board[6] != ' ' && m_board[6] == m_board[7] && m_board[7] == m_board[8]) { std::cout << m_board[6] << " is the winner!\n"; }
			else if (m_board[0] != ' ' && m_board[0] == m_board[3] && m_board[3] == m_board[6]) { std::cout << m_board[0] << " is the winner!\n"; }
			else if (m_board[1] != ' ' && m_board[1] == m_board[4] && m_board[4] == m_board[7]) { std::cout << m_board[1] << " is the winner!\n"; }
			else if (m_board[2] != ' ' && m_board[2] == m_board[5] && m_board[5] == m_board[8]) { std::cout << m_board[2] << " is the winner!\n"; }
			else if (m_board[0] != ' ' && m_board[0] == m_board[4] && m_board[4] == m_board[8]) { std::cout << m_board[0] << " is the winner!\n"; }
			else if (m_board[2] != ' ' && m_board[2] == m_board[4] && m_board[4] == m_board[6]) { std::cout << m_board[2] << " is the winner!\n"; }
			else { std::cout << "Tie, no winner this time\n"; }
		}
	}
};











